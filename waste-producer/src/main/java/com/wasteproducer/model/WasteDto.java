package com.wasteproducer.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class WasteDto {

    private Integer tankId;
    private Double fullness;
}
