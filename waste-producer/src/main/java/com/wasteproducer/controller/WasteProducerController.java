package com.wasteproducer.controller;

import com.wasteproducer.client.WasteProducerClient;
import com.wasteproducer.model.WasteDto;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class WasteProducerController {

    private final WasteProducerClient wasteProducerClient;

    @PostMapping("/throw")
    public void throwWaste(@RequestBody WasteDto wasteDto) {
        wasteProducerClient.throwWaste(wasteDto);
    }
}
