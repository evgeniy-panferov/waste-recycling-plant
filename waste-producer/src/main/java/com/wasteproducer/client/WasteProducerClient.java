package com.wasteproducer.client;

import com.wasteproducer.model.WasteDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("garbage-tank")
public interface WasteProducerClient {

    @PostMapping(path = "/garbage-tank/throw", consumes = "application/json")
    void throwWaste(@RequestBody WasteDto wasteDto);

}
