drop table if exists waste_trucks;

create table waste_trucks
(
    id               SERIAL           NOT NULL,
    capacity         DOUBLE PRECISION NOT NULL,
    place_of_service VARCHAR          NOT NULL,
    license_plate    VARCHAR          NOT NULL
)