package com.wastetruck;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class WasteTruckApplication {
    public static void main(String[] args) {
        SpringApplication.run(WasteTruckApplication.class, args);
    }
}
