package com.wastetruck;

import com.wastetruck.model.Truck;
import com.wastetruck.model.dto.TruckDto;

import java.util.List;
import java.util.stream.Collectors;

public class DtoConverter {

    private DtoConverter() {
    }

    public static Truck fromDTO(TruckDto truckDto) {
        Truck truck = new Truck();
        truck.setId(truckDto.getId());
        truck.setCapacity(truckDto.getCapacity());
        truck.setLicensePlate(truckDto.getLicensePlate());
        truck.setPlaceOfService(truckDto.getPlaceOfService());
        return truck;
    }

    public static TruckDto toDTO(Truck truck) {
        TruckDto truckDto = new TruckDto();
        truckDto.setId(truck.getId());
        truckDto.setCapacity(truck.getCapacity());
        truckDto.setLicensePlate(truck.getLicensePlate());
        truckDto.setPlaceOfService(truck.getPlaceOfService());
        return truckDto;
    }

    public static List<Truck> fromDTO(List<TruckDto> trucksDto) {
        return trucksDto.stream()
                .map(truckDto -> {
                    Truck truck = new Truck();
                    truck.setId(truckDto.getId());
                    truck.setCapacity(truckDto.getCapacity());
                    truck.setLicensePlate(truckDto.getLicensePlate());
                    truck.setPlaceOfService(truckDto.getPlaceOfService());
                    return truck;
                })
                .collect(Collectors.toList());
    }

    public static List<TruckDto> toDTO(List<Truck> trucks) {
        return trucks.stream()
                .map(truck -> {
                    TruckDto truckDto = new TruckDto();
                    truckDto.setId(truck.getId());
                    truckDto.setCapacity(truck.getCapacity());
                    truckDto.setLicensePlate(truck.getLicensePlate());
                    truckDto.setPlaceOfService(truck.getPlaceOfService());
                    return truckDto;
                })
                .collect(Collectors.toList());
    }

}
