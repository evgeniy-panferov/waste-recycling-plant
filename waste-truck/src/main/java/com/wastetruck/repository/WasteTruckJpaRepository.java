package com.wastetruck.repository;

import com.wastetruck.model.Truck;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WasteTruckJpaRepository extends JpaRepository<Truck, Integer> {

    @Query("select t.capacity from Truck t where t.id =:id")
    Double checkCapacity(@Param("id") int id);

    List<Truck> findByPlaceOfService(String city);
}
