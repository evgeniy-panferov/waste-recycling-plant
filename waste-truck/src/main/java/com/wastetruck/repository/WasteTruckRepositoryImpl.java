package com.wastetruck.repository;

import com.wastetruck.model.Truck;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
@AllArgsConstructor
public class WasteTruckRepositoryImpl implements WasteTruckRepository {

    private final WasteTruckJpaRepository wasteTruckJpaRepository;

    @Override
    public Truck save(Truck truck) {
        log.info("Сохраняем мусоровоз - {}", truck);
        return wasteTruckJpaRepository.save(truck);
    }

    @Override
    public List<Truck> getAll() {
        log.info("Получаем все мусоровозы");
        return wasteTruckJpaRepository.findAll();
    }


    @Override
    public void delete(int id) {
        log.info("Удаляем мусоровоз с id - {}", id);
        wasteTruckJpaRepository.deleteById(id);
    }

    @Override
    public Truck get(int id) {
        log.info("Получаем мусоровоз с id - {}", id);
        return wasteTruckJpaRepository.findById(id)
                .orElseThrow();

    }

    @Override
    public Truck findByCity(String city) {
        return wasteTruckJpaRepository.findByPlaceOfService(city).stream()
                .findAny()
                .orElseThrow();
    }

    @Override
    public Double checkCapacity(int id) {
        log.info("Проверяем емкость бункера с id - {}", id);
        return wasteTruckJpaRepository.checkCapacity(id);
    }


}
