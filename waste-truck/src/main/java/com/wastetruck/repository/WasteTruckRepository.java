package com.wastetruck.repository;

import com.wastetruck.model.Truck;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WasteTruckRepository {

    Truck save(Truck truck);

    List<Truck> getAll();

    void delete(int id);

    Truck get(int id);

    Truck findByCity(String city);

    Double checkCapacity(int id);

}
