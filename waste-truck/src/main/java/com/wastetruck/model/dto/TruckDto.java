package com.wastetruck.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TruckDto {

    private Integer id;
    private Double capacity;
    private Double fullness;
    private String placeOfService;
    private String licensePlate;

}
