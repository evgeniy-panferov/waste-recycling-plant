package com.wastetruck.controller;

import com.wastetruck.client.WasteTruckToGarbageTankClient;
import com.wastetruck.model.dto.GarbageTankDto;
import com.wastetruck.model.dto.TruckDto;
import com.wastetruck.repository.WasteTruckRepositoryImpl;
import com.wastetruck.service.WasteTruckService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.wastetruck.DtoConverter.fromDTO;
import static com.wastetruck.DtoConverter.toDTO;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = "/waste-truck")
public class WasteTruckController {

    private final WasteTruckRepositoryImpl wasteTruckRepository;
    private final WasteTruckToGarbageTankClient wasteTruckClient;
    private final WasteTruckService wasteTruckService;

    @PostMapping(value = "/add")
    public TruckDto add(@RequestBody TruckDto truck) {
        log.info("Обработка запроса add");
        return toDTO(wasteTruckRepository.save(fromDTO(truck)));
    }

    @GetMapping(value = "/find")
    public void findTruck(@RequestBody GarbageTankDto garbageTankDto) {
        log.info("Обработка запроса find");
        GarbageTankDto cleanGarbageTank = wasteTruckService.cleanGarbageTank(garbageTankDto);
        wasteTruckClient.updateGarbageTank(cleanGarbageTank);
    }

    @PostMapping(value = "/update")
    public TruckDto update(@RequestBody TruckDto truck) {
        log.info("Обработка запроса update");
        return toDTO(wasteTruckRepository.save(fromDTO(truck)));
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable int id) {
        log.info("Обработка запроса delete");
        wasteTruckRepository.delete(id);
    }

    @GetMapping(value = "/{id}")
    public TruckDto get(@PathVariable int id) {
        log.info("Обработка запроса get");
        return toDTO(wasteTruckRepository.get(id));
    }

    @GetMapping(value = "/trucks")
    public List<TruckDto> getAll() {
        log.info("Обработка запроса getAll");
        return toDTO(wasteTruckRepository.getAll());
    }

    @GetMapping(value = "/capacity/{id}")
    public Double checkCapacity(@PathVariable int id) {
        log.info("Обработка запроса checkCapacity");
        return wasteTruckRepository.checkCapacity(id);
    }
}
