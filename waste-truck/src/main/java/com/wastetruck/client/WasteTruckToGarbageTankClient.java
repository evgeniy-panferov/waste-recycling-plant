package com.wastetruck.client;

import com.wastetruck.model.dto.GarbageTankDto;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient("garbage-tank")
public interface WasteTruckToGarbageTankClient {

    void updateGarbageTank(GarbageTankDto garbageTankDto);
}
