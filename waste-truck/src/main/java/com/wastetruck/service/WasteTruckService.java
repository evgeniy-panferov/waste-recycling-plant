package com.wastetruck.service;

import com.wastetruck.model.Truck;
import com.wastetruck.model.dto.GarbageTankDto;
import com.wastetruck.repository.WasteTruckRepositoryImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class WasteTruckService {

    private final WasteTruckRepositoryImpl wasteTruckRepository;

    public GarbageTankDto cleanGarbageTank(GarbageTankDto garbageTankDto){
        Truck truck = wasteTruckRepository.findByCity(garbageTankDto.getCity());
        double diff = garbageTankDto.getFullness() - truck.getCapacity();
        if (diff > 0) {
            garbageTankDto.setFullness(diff);
        } else {
            garbageTankDto.setFullness(0.0);
        }
        return garbageTankDto;
    }
}
