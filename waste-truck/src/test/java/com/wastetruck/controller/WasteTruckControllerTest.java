package com.wastetruck.controller;

import com.wastetruck.model.dto.TruckDto;
import com.wastetruck.repository.WasteTruckRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static com.wastetruck.DtoConverter.fromDTO;
import static com.wastetruck.TruckTestData.getSingleTruckDto;
import static com.wastetruck.TruckTestData.getTruckDtoList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class WasteTruckControllerTest {

    @Autowired
    private WasteTruckController wasteTruckController;

    @MockBean
    private WasteTruckRepositoryImpl wasteTruckRepository;

    @Test
    void add() {
        when(wasteTruckRepository.save(fromDTO(getSingleTruckDto())))
                .thenReturn(fromDTO(getSingleTruckDto()));

        TruckDto add = wasteTruckController.add(getSingleTruckDto());
        assertEquals(getSingleTruckDto(), add);
    }

    @Test
    void update() {
        when(wasteTruckRepository.save(fromDTO(getSingleTruckDto())))
                .thenReturn(fromDTO(getSingleTruckDto()));

        TruckDto add = wasteTruckController.add(getSingleTruckDto());
        assertEquals(getSingleTruckDto(), add);
    }

    @Test
    void delete() {
        wasteTruckController.delete(1);

        verify(wasteTruckRepository, Mockito.times(1))
                .delete(1);
    }

    @Test
    void get() {
        when(wasteTruckRepository.get(1))
                .thenReturn(fromDTO(getSingleTruckDto()));

        TruckDto truckDto = wasteTruckController.get(1);
        assertEquals(getSingleTruckDto(), truckDto);
    }

    @Test
    void getAll() {
        when(wasteTruckRepository.getAll())
                .thenReturn(fromDTO(getTruckDtoList()));

        List<TruckDto> trucksDto = wasteTruckController.getAll();
        assertEquals(getTruckDtoList(), trucksDto);
    }

    @Test
    void checkCapacity() {
        when(wasteTruckRepository.checkCapacity(1))
                .thenReturn(10.0);

        Double aDouble = wasteTruckController.checkCapacity(1);
        assertEquals(10, aDouble);
    }
}