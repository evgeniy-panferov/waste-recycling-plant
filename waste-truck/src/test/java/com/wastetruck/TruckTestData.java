package com.wastetruck;

import com.wastetruck.model.dto.TruckDto;

import java.util.Arrays;
import java.util.List;

public class TruckTestData {

    public static TruckDto getSingleTruckDto() {
        TruckDto truckDto = new TruckDto();
        truckDto.setId(1);
        truckDto.setCapacity(10.0);
        truckDto.setFullness(0.0);
        truckDto.setLicensePlate("E777E99");
        truckDto.setPlaceOfService("Moscow");
        return truckDto;
    }

    public static TruckDto getSingleTruckDto(Integer id, Double capacity, Double fullness, String licensePlate, String placeOfService) {
        TruckDto truckDto = new TruckDto();
        truckDto.setId(id);
        truckDto.setCapacity(capacity);
        truckDto.setFullness(fullness);
        truckDto.setLicensePlate(licensePlate);
        truckDto.setPlaceOfService(placeOfService);
        return truckDto;
    }

    public static List<TruckDto> getTruckDtoList() {
        return Arrays.asList(
                getSingleTruckDto(1, 10.0, 0.0, "E777E99", "Moscow"),
                getSingleTruckDto(2, 12.0, 0.0, "E123E71", "Tula")
        );
    }
}
