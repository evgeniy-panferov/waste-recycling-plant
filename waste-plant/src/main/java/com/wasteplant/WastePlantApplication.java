package com.wasteplant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class WastePlantApplication {
    public static void main(String[] args) {
        SpringApplication.run(WastePlantApplication.class, args);
    }
}
