package com.wasteplant.repository;

import com.wasteplant.model.Plant;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantRepository {

    Plant findById(Long id);

    List<Plant> findByCity(String city);

    int save(Plant plant);

    int update(Plant plant);

    int deleteById(Long id);

    List<Plant> findAll();
}
