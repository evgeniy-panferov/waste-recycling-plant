package com.wasteplant.repository;

import com.wasteplant.model.Plant;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@AllArgsConstructor
@Repository
public class JdbcPlantRepository implements PlantRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Plant findById(Long id) {
        log.info("Поиск завода с id - {}", id);
        return namedParameterJdbcTemplate.queryForObject("select * from Plant where id = :id",
                new MapSqlParameterSource("id", id), new BeanPropertyRowMapper<>(Plant.class));
    }

    @Override
    public List<Plant> findByCity(String city) {
        log.info("Поиск заводов по расположению - {}", city);
        return namedParameterJdbcTemplate.query("select * from Plant where city = :city",
                new MapSqlParameterSource("city", city.toLowerCase()), new BeanPropertyRowMapper<>(Plant.class));
    }

    @Override
    @Transactional
    public int save(Plant plant) {
        log.info("Сохранение завода в БД - {}", plant);
        return namedParameterJdbcTemplate.update("insert into Plant(capacity, recycling_speed, city) " +
                        "values(:capacity, :recyclingSpeed, :city)",
                new BeanPropertySqlParameterSource(plant));
    }

    @Override
    @Transactional
    public int update(Plant plant) {
        log.info("Обновление завода - {}", plant);
        return namedParameterJdbcTemplate.update("update Plant SET capacity = :capacity, recycling_speed = :recyclingSpeed, city = :city where id = :id",
                new BeanPropertySqlParameterSource(plant));
    }

    @Override
    public int deleteById(Long id) {
        log.info("Удаляется завод с id-{}", id);
        return namedParameterJdbcTemplate.update("delete from Plant where id=:id",
                new MapSqlParameterSource("id", id));
    }

    @Override
    public List<Plant> findAll() {
        log.info("Найти все заводы");
        return namedParameterJdbcTemplate.query("select * from Plant", new BeanPropertyRowMapper<>(Plant.class));
    }
}
