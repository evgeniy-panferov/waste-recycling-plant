package com.wasteplant.controller;

import com.wasteplant.model.Plant;
import com.wasteplant.model.dto.WasteDto;
import com.wasteplant.repository.JdbcPlantRepository;
import com.wasteplant.service.RecycleService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = "/waste-plant")
public class Controller {

    private final RecycleService recycleService;
    private final JdbcPlantRepository jdbcPlantRepository;

    @PostMapping(value = "/recycle")
    public String recycling(@RequestBody WasteDto wasteDto) {
        log.info("Обработка запроса recycling с телом - {}", wasteDto);
        return recycleService.recycle(wasteDto);
    }

    @DeleteMapping(value = "/{id}")
    public int delete(@PathVariable long id) {
        log.info("Удаляю объект с id - {}", id);
        return jdbcPlantRepository.deleteById(id);
    }

    @PostMapping(value = "/add")
    public int add(@RequestBody Plant plant) {
        log.info("Добавляю завод - {}", plant);
        return jdbcPlantRepository.save(plant);
    }

    @PostMapping(value = "/update")
    public int update(@RequestBody Plant plant) {
        log.info("Обновляю завод - {}", plant);
        return jdbcPlantRepository.update(plant);
    }

    @GetMapping(value = "/plants")
    public List<Plant> findAll() {
        log.info("Получаю все заводы");
        return jdbcPlantRepository.findAll();
    }

    @GetMapping(value = "plants/{city}")
    public List<Plant> findByCity(@PathVariable String city) {
        log.info("Получаю все заводы в городе - {}", city);
        return jdbcPlantRepository.findByCity(city.toLowerCase());
    }

    @GetMapping(value = "/plant/{id}")
    public Plant findById(@PathVariable Long id) {
        log.info("Получаю завод с id - {}", id);
        return jdbcPlantRepository.findById(id);
    }

}
