package com.wasteplant.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Plant {
    private Long id;
    private Double recyclingSpeed;
    private Double capacity;
    private String city;
}
