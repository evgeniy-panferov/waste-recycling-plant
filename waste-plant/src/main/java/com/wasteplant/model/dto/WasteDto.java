package com.wasteplant.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WasteDto {
    private Double size;
    private String city;
}
