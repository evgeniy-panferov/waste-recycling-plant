package com.wasteplant.service;

import com.wasteplant.model.Plant;
import com.wasteplant.model.dto.WasteDto;
import com.wasteplant.repository.JdbcPlantRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RecycleService {

    private final JdbcPlantRepository jdbcPlantRepository;

    public String recycle(WasteDto wasteDto) {
        Plant plant = jdbcPlantRepository.findByCity(wasteDto.getCity().toLowerCase()).stream()
                .findFirst()
                .orElseThrow();
        Double recyclingSpeed = plant.getRecyclingSpeed();
        Double size = wasteDto.getSize();
        double recycleTime = size / recyclingSpeed * 24;
        return String.format("Переработка займет %.2f часов", recycleTime);
    }

}
