drop table if exists plant;

create table plant
(
    id              serial  not null,
    capacity        double precision,
    recycling_speed double precision,
    city            varchar not null
);