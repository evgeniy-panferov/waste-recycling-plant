package com.wasteplant.service;

import com.wasteplant.repository.JdbcPlantRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static com.wasteplant.service.PlantData.getPlants;
import static com.wasteplant.service.WasteData.getWaste;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
class RecycleServiceTest {

    @Autowired
    private RecycleService recycleService;

    @MockBean
    private JdbcPlantRepository jdbcPlantRepository;

    @Test
    void recycle() {
        when(jdbcPlantRepository.findByCity("moscow"))
                .thenReturn(getPlants());
        String recycle = recycleService.recycle(getWaste());
        assertEquals("Переработка займет 2.40 часов", recycle);
    }
}