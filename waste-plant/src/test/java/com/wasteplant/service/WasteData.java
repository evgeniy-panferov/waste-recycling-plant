package com.wasteplant.service;

import com.wasteplant.model.dto.WasteDto;

public class WasteData {

    public static WasteDto getWaste() {
        WasteDto wasteDto = new WasteDto();
        wasteDto.setCity("moscow");
        wasteDto.setSize(10.0);
        return wasteDto;
    }
}
