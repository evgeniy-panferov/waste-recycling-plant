package com.wasteplant.service;

import com.wasteplant.model.Plant;

import java.util.Arrays;
import java.util.List;

public class PlantData {

    public static List<Plant> getPlants() {
        Plant plant = new Plant();
        plant.setId(1L);
        plant.setCapacity(1000.0);
        plant.setCity("moscow");
        plant.setRecyclingSpeed(100.0);
        return Arrays.asList(plant);
    }
}
