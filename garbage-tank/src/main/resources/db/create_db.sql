drop table if exists garbage_tanks;

create table garbage_tanks
(
    id       SERIAL           NOT NULL,
    city     VARCHAR          NOT NULL,
    street   VARCHAR          NOT NULL,
    capacity DOUBLE PRECISION NOT NULL
)