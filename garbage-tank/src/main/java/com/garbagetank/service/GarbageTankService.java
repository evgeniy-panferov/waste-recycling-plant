package com.garbagetank.service;

import com.garbagetank.client.GarbageTankClient;
import com.garbagetank.model.dto.GarbageTankDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class GarbageTankService {

    private final GarbageTankClient garbageTankClient;

    public void tankIsFull(GarbageTankDto garbageTank) {
        double tankIsFull = garbageTank.getFullness() / garbageTank.getCapacity() * 100;
        if (tankIsFull >= 80) {
            garbageTankClient.full(garbageTank);
        }
    }
}
