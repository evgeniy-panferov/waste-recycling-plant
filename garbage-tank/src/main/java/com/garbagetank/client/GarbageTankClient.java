package com.garbagetank.client;

import com.garbagetank.model.dto.GarbageTankDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("waste-truck")
public interface GarbageTankClient {

    @GetMapping(path = "/waste-truck/find")
    void full(@RequestBody GarbageTankDto garbageTankDto);
}
