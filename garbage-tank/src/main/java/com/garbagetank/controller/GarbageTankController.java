package com.garbagetank.controller;

import com.garbagetank.model.dto.GarbageTankDto;
import com.garbagetank.model.dto.WasteDto;
import com.garbagetank.repository.GarbageTankRepositoryImpl;
import com.garbagetank.service.GarbageTankService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.garbagetank.GarbageTankDtoConverter.*;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = "/garbage-tank")
public class GarbageTankController {

    private final GarbageTankRepositoryImpl garbageTankRepository;
    private final GarbageTankService garbageTankService;

    @PostMapping("/add")
    public GarbageTankDto add(@RequestBody GarbageTankDto tank) {
        log.info("Обработка запроса add - {}", tank);
        return toDto(garbageTankRepository.save(fromDto(tank)));
    }

    @PostMapping("/throw")
    public GarbageTankDto throwIntoTank(@RequestBody WasteDto wasteDto) {
        log.info("Обработка запроса throw - {}", wasteDto);
        GarbageTankDto garbageTank = toDto(garbageTankRepository.throwIntoTank(wasteDto));
        garbageTankService.tankIsFull(garbageTank);
        return garbageTank;
    }

    @PostMapping("/update")
    public GarbageTankDto update(@RequestBody GarbageTankDto tank) {
        log.info("Обработка запроса update tank -{}", tank);
        return toDto(garbageTankRepository.save(fromDto(tank)));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        log.info("Обработка запроса delete id - {}", id);
        garbageTankRepository.delete(id);
    }

    @GetMapping(value = "/{id}")
    public GarbageTankDto get(@PathVariable int id) {
        log.info("Обработка запроса get id - {}", id);
        return toDto(garbageTankRepository.get(id));
    }

    @GetMapping(value = "tanks")
    public List<GarbageTankDto> getAll() {
        log.info("Обработка запроса getall");
        return toDtoList(garbageTankRepository.getAll());
    }

}
