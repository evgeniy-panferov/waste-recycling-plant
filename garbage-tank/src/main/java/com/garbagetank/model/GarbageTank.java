package com.garbagetank.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Table(name = "garbage_tanks")
@NoArgsConstructor
@Data
public class GarbageTank {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String city;
    private String street;
    private Double capacity;
    private Double fullness;

}
