package com.garbagetank.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class WasteDto {

    private Integer tankId;
    private Double fullness;
}
