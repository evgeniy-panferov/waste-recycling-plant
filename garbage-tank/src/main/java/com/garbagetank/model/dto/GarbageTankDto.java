package com.garbagetank.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GarbageTankDto {
    private Integer id;
    private String city;
    private String street;
    private Double capacity;
    private Double fullness;
}
