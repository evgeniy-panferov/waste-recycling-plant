package com.garbagetank;

import com.garbagetank.model.GarbageTank;
import com.garbagetank.model.dto.GarbageTankDto;

import java.util.List;
import java.util.stream.Collectors;

public class GarbageTankDtoConverter {

    private GarbageTankDtoConverter() {
    }

    public static GarbageTankDto toDto(GarbageTank garbageTank) {
        GarbageTankDto garbageTankDto = new GarbageTankDto();
        garbageTankDto.setId(garbageTank.getId());
        garbageTankDto.setCapacity(garbageTank.getCapacity());
        garbageTankDto.setCity(garbageTank.getCity());
        garbageTankDto.setStreet(garbageTank.getStreet());
        return garbageTankDto;
    }

    public static GarbageTank fromDto(GarbageTankDto garbageTankDto) {
        GarbageTank garbageTank = new GarbageTank();
        garbageTank.setId(garbageTankDto.getId());
        garbageTank.setCapacity(garbageTankDto.getCapacity());
        garbageTank.setCity(garbageTankDto.getCity());
        garbageTank.setStreet(garbageTankDto.getStreet());
        return garbageTank;
    }

    public static List<GarbageTankDto> toDtoList(List<GarbageTank> garbageTanks) {
        return garbageTanks.stream()
                .map(garbageTank -> {
                    GarbageTankDto garbageTankDto = new GarbageTankDto();
                    garbageTankDto.setId(garbageTank.getId());
                    garbageTankDto.setCapacity(garbageTank.getCapacity());
                    garbageTankDto.setCity(garbageTank.getCity());
                    garbageTankDto.setStreet(garbageTank.getStreet());
                    return garbageTankDto;
                })
                .collect(Collectors.toList());
    }

    public static List<GarbageTank> fromDtoList(List<GarbageTankDto> garbageTanksDto) {
        return garbageTanksDto.stream()
                .map(garbageTankDto -> {
                    GarbageTank garbageTank = new GarbageTank();
                    garbageTank.setId(garbageTankDto.getId());
                    garbageTank.setCapacity(garbageTankDto.getCapacity());
                    garbageTank.setCity(garbageTankDto.getCity());
                    garbageTank.setStreet(garbageTankDto.getStreet());
                    return garbageTank;
                })
                .collect(Collectors.toList());
    }

}
