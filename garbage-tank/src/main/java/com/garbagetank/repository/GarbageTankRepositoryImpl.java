package com.garbagetank.repository;

import com.garbagetank.model.GarbageTank;
import com.garbagetank.model.dto.WasteDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Slf4j
@Repository
@AllArgsConstructor
public class GarbageTankRepositoryImpl implements GarbageTankRepository {

    private final GarbageTankJpaRepository garbageTankJpaRepository;

    @Override
    public GarbageTank save(GarbageTank tank) {
        log.info("Добавляю мусорный контейнер - {}", tank);
        return garbageTankJpaRepository.save(tank);
    }

    public GarbageTank throwIntoTank(WasteDto wasteDto) {
        log.info("Наполняю мусорный контейнер - {}", wasteDto);
        Integer tankId = wasteDto.getTankId();
        GarbageTank garbageTank = get(tankId);
        garbageTank.setFullness(garbageTank.getFullness() + wasteDto.getFullness());
        return garbageTankJpaRepository.save(garbageTank);
    }

    @Override
    public List<GarbageTank> getAll() {
        log.info("Получаю все мусорные контейнеры");
        return garbageTankJpaRepository.findAll();
    }

    @Override
    public void delete(int id) {
        log.info("Удаляю мусорный контейнер с id - {}", id);
        garbageTankJpaRepository.deleteById(id);
    }

    @Override
    public GarbageTank get(int id) {
        log.info("Получаю мусорный контейнер с id - {}", id);
        Optional<GarbageTank> garbageTank = garbageTankJpaRepository.findById(id);
        return garbageTank.orElseThrow();
    }

}
