package com.garbagetank.repository;

import com.garbagetank.model.GarbageTank;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GarbageTankRepository {

    GarbageTank save(GarbageTank tank);

    List<GarbageTank> getAll();

    void delete(int id);

    GarbageTank get(int id);

}
