package com.garbagetank.repository;

import com.garbagetank.model.GarbageTank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GarbageTankJpaRepository extends JpaRepository<GarbageTank, Integer> {
}
