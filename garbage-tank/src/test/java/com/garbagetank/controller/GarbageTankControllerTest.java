package com.garbagetank.controller;

import com.garbagetank.model.GarbageTank;
import com.garbagetank.model.dto.GarbageTankDto;
import com.garbagetank.repository.GarbageTankRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static com.garbagetank.GarbageTankDtoConverter.*;
import static com.garbagetank.GarbageTankTestData.getSeveralTanks;
import static com.garbagetank.GarbageTankTestData.getSingleTank;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class GarbageTankControllerTest {

    @Autowired
    private GarbageTankController garbageTankController;

    @MockBean
    private GarbageTankRepositoryImpl garbageTankRepository;

    @Test
    void add() {
        GarbageTank singleTank = getSingleTank();
        when(garbageTankRepository.save(singleTank))
                .thenReturn(singleTank);


        GarbageTankDto tank = garbageTankController.add(toDto(getSingleTank()));
        assertEquals(toDto(singleTank), tank);
    }

    @Test
    void update() {
        GarbageTank singleTank = getSingleTank();
        when(garbageTankRepository.save(singleTank))
                .thenReturn(singleTank);


        GarbageTankDto tank = garbageTankController.add(toDto(getSingleTank()));
        assertEquals(toDto(singleTank), tank);
    }

    @Test
    void delete() {
        garbageTankController.delete(1);

        verify(garbageTankRepository, Mockito.times(1))
                .delete(1);
    }


    @Test
    void get() {
        when(garbageTankRepository.get(0))
                .thenReturn(getSingleTank());

        GarbageTank garbageTank = fromDto((garbageTankController.get(0)));
        assertEquals(getSingleTank(), garbageTank);

    }

    @Test
    void getAll() {
        when(garbageTankRepository.getAll())
                .thenReturn(getSeveralTanks());
        List<GarbageTank> garbageTanks = fromDtoList(garbageTankController.getAll());
        assertEquals(getSeveralTanks(), garbageTanks);
    }

}