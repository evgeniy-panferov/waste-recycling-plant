package com.garbagetank;

import com.garbagetank.model.GarbageTank;

import java.util.Arrays;
import java.util.List;

public class GarbageTankTestData {

    private GarbageTankTestData() {
    }

    public static GarbageTank getSingleTank() {
        GarbageTank garbageTank = new GarbageTank();
        garbageTank.setId(0);
        garbageTank.setCity("Moscow");
        garbageTank.setStreet("Krasnodarskaya street");
        garbageTank.setCapacity(0.75);
        return garbageTank;
    }

    public static GarbageTank getSingleTank(Integer id, String city, String street, Double capacity, Double fullness) {
        GarbageTank garbageTank = new GarbageTank();
        garbageTank.setId(id);
        garbageTank.setCity(city);
        garbageTank.setStreet(street);
        garbageTank.setCapacity(capacity);
        return garbageTank;
    }

    public static List<GarbageTank> getSeveralTanks() {
        return Arrays.asList(
                getSingleTank(0, "Moscow", "Krasnodarskaya street", 0.75, 0.0),
                getSingleTank(1, "Tula", "Lenina street", 1.5, 1.3)
        );
    }


}
